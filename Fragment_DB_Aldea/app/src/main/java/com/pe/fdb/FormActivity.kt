package com.pe.fdb

import android.os.Bundle
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import com.pe.fdb.util.AppPreferences
import kotlinx.android.synthetic.main.activity_form.*

class FormActivity : AppCompatActivity(), CompoundButton.OnCheckedChangeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)



        chkRecordar.setOnCheckedChangeListener(this)
        if (AppPreferences.nameUser!!.isNotEmpty()) {
            eteUsuario.setText(AppPreferences.nameUser)
            chkRecordar.isChecked = true
        }


        /*val preferences: SharedPreferences = getSharedPreferences("appPreference", Context.MODE_PRIVATE)
        val editor = preferences.edit()


        editor.putBoolean("mostrarPopUp", false)
        editor.putString("nombreUsuario", "Riccardo Mija")
        editor.putLong("anchoVista", 4L)

        editor.apply()


        val nombre = preferences.getString("nombreUsuario", "No se encontro el valor")
        val mostrarPopUp = preferences.getBoolean("mostrarPopUp", false)

        editor.remove("nombreUsuario") //Borrar Llave nombreUsuario
        editor.remove("mostrarPopUp") //Borrar Llave mostrarPopUp
        editor.apply()

        //Limpiamos el contenido del sharePreference
        editor.clear()
        editor.apply()

*/


    }

    override fun onCheckedChanged(compoundButton: CompoundButton, isChecked: Boolean) {
        if (isChecked) {
            AppPreferences.nameUser = eteUsuario.text.toString()
        } else {

            AppPreferences.nameUser = ""
        }
    }
}