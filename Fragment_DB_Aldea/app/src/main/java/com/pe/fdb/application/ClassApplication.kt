package com.pe.fdb.application

import android.app.Application
import com.pe.fdb.util.AppPreferences

class ClassApplication : Application() {


    override fun onCreate() {
        super.onCreate()

        //Instance Class to manage Shared Preference
        AppPreferences.init(this)

    }


}
