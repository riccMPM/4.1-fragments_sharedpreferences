package com.pe.fdb

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_fragment.*

class FragmentActivity : AppCompatActivity(), View.OnClickListener, OnPrimerFragmentListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment)

        butUno.setOnClickListener(this)
        butDos.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.butUno) {
            loadFragment(PrimerFragment())
        }
    }

    private fun loadFragment(fragment: PrimerFragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.flaFragment, fragment)
        fragmentTransaction.commit()
    }


    override fun onBotonSeleccionado(mensaje: String) {
        tviTexto.text = mensaje
    }
}