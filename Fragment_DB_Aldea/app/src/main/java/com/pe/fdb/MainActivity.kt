package com.pe.fdb

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        butFragment.setOnClickListener(this)
        butBD.setOnClickListener(this)
    }

    override fun onClick(view: View) {

        when (view.id) {
            R.id.butFragment -> {
                val intent = Intent(this, FragmentActivity::class.java)
                startActivity(intent)
            }
            else -> {
                val intent = Intent(this, FormActivity::class.java)
                startActivity(intent)
            }
        }

    }
}